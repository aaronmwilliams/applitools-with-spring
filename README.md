# applitools-with-spring Readme

### Purpose of project
To demonstrate a simple Applitools framework using Spring and Gradle.

### Installing and Running

##### Applitools Account
You will need an Applitools account. Keep note of your API Key as this is needed in the configuration mentioned below.

##### Checking Out Project

Import as a **GRADLE** project.

### Running Tests

##### Gradle
After the project has built you can run the tests using the Gradle wrapper commands.

```
./gradlew clean build -Dapi.key="[ENTER_YOUR_APPLITOOLS_API_KEY]" -Dwebdriver.chrome.driver="[ENTER_PATH_TO_CHROME_DRIVER]"
```
or
```
./gradlew test -Dapi.key="[ENTER_YOUR_APPLITOOLS_API_KEY]" -Dwebdriver.chrome.driver="[ENTER_PATH_TO_CHROME_DRIVER]"
```

##### jUnit
To run the tests using jUnit you will need to use the following VM options. Set these as your default jUnit run configuration.
```
-ea
-Dapi.key="[ENTER_YOUR_APPLITOOLS_API_KEY]
-Dwebdriver.chrome.driver=[THE PATH TO YOUR CHROMEDRIVER.EXE]
```

##### Jenkins
You can run on Jenkins with the Appitools Eyes plugin. Just link to your Git repository and run the Gradle build.