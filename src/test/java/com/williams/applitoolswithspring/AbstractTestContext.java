package com.williams.applitoolswithspring;

import com.applitools.eyes.selenium.Eyes;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(classes = TestConfiguration.class)
public abstract class AbstractTestContext {

    @Autowired
    protected WebDriver driver;

    @Autowired
    protected Eyes eyes;

    public AbstractTestContext goToPage(String url) {
        driver.get(url);
        return this;
    }

    public AbstractTestContext performVisualCheck(String app, String step) {
        eyes.open(driver, app, step);
        eyes.checkWindow();
        eyes.close();
        return this;
    }

    public AbstractTestContext resizeToMobile() {
        driver.manage().window().setSize(new Dimension(375, 812));
        return this;
    }

}
