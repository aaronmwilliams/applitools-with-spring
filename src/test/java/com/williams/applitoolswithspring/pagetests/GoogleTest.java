package com.williams.applitoolswithspring.pagetests;

import com.williams.applitoolswithspring.AbstractTestContext;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class GoogleTest extends AbstractTestContext {

    @Test
    public void shouldMatchGoogleDesktop() {
        goToPage("http://www.google.com/")
                .performVisualCheck("Google", "Google Main Desktop Page");
    }

}
