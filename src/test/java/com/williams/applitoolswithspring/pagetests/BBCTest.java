package com.williams.applitoolswithspring.pagetests;

import com.williams.applitoolswithspring.AbstractTestContext;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class BBCTest extends AbstractTestContext {

    private static final String BBC_CONTACT = "http://www.bbc.co.uk/contact";

    @Test
    public void shouldMatchAmazonDesktop() {
        goToPage(BBC_CONTACT)
                .performVisualCheck("BBC", "BBC Desktop Contact Page");
    }

    @Test
    public void shouldMatchAmazonMobile() {
        goToPage(BBC_CONTACT)
                .resizeToMobile()
                .performVisualCheck("BBC", "BBC Mobile Contact Page");
    }

}
