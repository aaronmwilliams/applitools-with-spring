package com.williams.applitoolswithspring;

import com.applitools.eyes.BatchInfo;
import com.applitools.eyes.selenium.Eyes;
import com.williams.applitoolswithspring.infrastructure.EnvironmentProperties;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TestConfiguration {

    @Autowired
    Eyes eyes;

    @Autowired
    WebDriver driver;

    @Bean
    public Eyes buildEyes() {
        Eyes eyes = new Eyes();
        eyes.setApiKey(System.getProperty("api.key"));
        eyes.setBatch(createBatch());
        return eyes;
    }

    @Bean
    public WebDriver buildWebDriver() {
        driver = new ChromeDriver();
        return driver;
    }

    private BatchInfo createBatch() {
        BatchInfo batch = new BatchInfo(EnvironmentProperties.getEnvID());
        batch.setId(EnvironmentProperties.getEnvName());
        return batch;
    }

}
