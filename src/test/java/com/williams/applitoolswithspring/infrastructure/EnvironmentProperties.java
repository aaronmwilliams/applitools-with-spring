package com.williams.applitoolswithspring.infrastructure;

import java.text.SimpleDateFormat;
import java.util.Date;

public class EnvironmentProperties {

    public static String getEnvName() {
        return checkIfExists("APPLITOOLS_BATCH_ID");
    }

    public static String getEnvID() {
        return checkIfExists("JOB_NAME");
    }

    private static String checkIfExists(String property) {
        try {
            System.getenv(property).isEmpty();
        } catch (Exception e) {
            return createBatchName();
        }
        return System.getenv(property);
    }

    private static String createBatchName() {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-YY HH:mm:ss");
        return "Test Run: " + sdf.format(date);
    }

}
