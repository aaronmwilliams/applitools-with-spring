package com.williams.applitoolswithspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApplitoolsWithSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApplitoolsWithSpringApplication.class, args);
	}
}
